# Configurations for makefile.
# Edit this file to specify the paths and versions of required tools and directories.

export os := $(shell uname)

ifeq ($(os),Darwin)
	include ./env.mac
endif

ifeq ($(os),Linux)
	include ./env.linux
endif

# Versions produced by this build:
export VERSION = 0.1
