# This makefile contains no information about file structure or tool locations.
# All such configurations should be made in makefile.inc

include makefile.inc

.ONESHELL:
.SUFFIXES:

# Artifact names:
export PRODUCT_NAME := Java Utilities
export Description := Java Utilities

# Command aliases:
export SHELL := /bin/sh
export JAVA := $(JAVA_HOME)/bin/java

# Relative locations:
export ThisDir := $(shell pwd)
export util_classes := $(ThisDir)/maven/classes
export javadoc_dir := javadocs


################################################################################
# Tasks


.PHONY: all mvnversion jars test javadoc clean info

all: test install javadoc

mvnversion:
	mvn --version


# ------------------------------------------------------------------------------
# Package into Jar files and install in local maven repository.
install:
	mvn clean install


# ------------------------------------------------------------------------------
# For development.
test:
	mvn test


# ------------------------------------------------------------------------------
# Generate javadocs for all modules.
javadoc:
	mvn javadoc:javadoc


# ------------------------------------------------------------------------------
clean:
	mvn clean

info:
	@echo "Makefile for $(PRODUCT_NAME)"
