package cliffberg.utilities;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.FileVisitResult;
import java.nio.file.attribute.BasicFileAttributes;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class PathUtilities<TId> {
	
	public interface FileOperator {
		void operateOnFile(Path f) throws IOException;
	}
	
	/**
	 Delete all of the files and subdirectories in the specified directory, as
	 well as the specified directory.
	 */
	public static void deleteDirectoryTree(File dir) throws Exception {
		operateOnDirectoryTree(dir, new FileOperator() {
			public void operateOnFile(Path f) throws IOException {
				Files.delete(f);
			}
		});
	}
	
	/**
	 
	 */
	public static void printDirectoryTree(File dir) throws Exception {
		operateOnDirectoryTree(dir, new FileOperator() {
			public void operateOnFile(Path f) throws IOException {
				System.out.println(f.toString());
			}
		});
	}
	
	/**
	 
	 */
	public static void operateOnDirectoryTree(File dir, FileOperator operator) throws Exception {
		if (! dir.exists()) return;
		if (! dir.isDirectory()) throw new Exception("File " + dir.toString() + " is not a direcrtory");
		Files.walkFileTree(dir.toPath(), new SimpleFileVisitor<Path> () {
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				operator.operateOnFile(file);
				return FileVisitResult.CONTINUE;
			}
			
			public FileVisitResult postVisitDirectory(Path d, IOException exc) throws IOException {
				if (exc == null) {
					operator.operateOnFile(d);
					return FileVisitResult.CONTINUE;
				} else {
					throw exc; // directory iteration failed
				}
			}
		});
	}

	/**
	 Recursively delete the specified directory and all contents.
	 */
	public static void deleteDirectoryTree(Path root) throws Exception {
		
		Files.walkFileTree(root, new SimpleFileVisitor<Path>() {
			
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
			throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}
			
			public FileVisitResult postVisitDirectory(Path dir, IOException e)
			throws IOException {
				if (e == null) {
					Files.delete(dir);
					return FileVisitResult.CONTINUE;
				} else {
					// directory iteration failed
					throw e;
				}
			}
		});
	}
}
